% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/stats_pheno.R
\name{stats_pheno}
\alias{stats_pheno}
\title{Statistics on cleaned phenotyping data}
\usage{
stats_pheno(path_to_pheno, ..., path_to_output)
}
\arguments{
\item{path_to_pheno}{path to cleaned phenotyping file (with the extension).}

\item{...}{variable(s) to use as counting group(s).}

\item{path_to_output}{where to save the stats (if NULL, table is displayed in console)}
}
\value{
table of counts for traits
}
\description{
Statistics on cleaned phenotyping data
}
\examples{
\dontrun{
}
}
