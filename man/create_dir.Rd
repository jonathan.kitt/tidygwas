% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/create_dir.R
\name{create_dir}
\alias{create_dir}
\title{Create folders for data and results}
\usage{
create_dir(df, ..., data_to, results_to)
}
\arguments{
\item{df}{table from which the paths will be created.}

\item{...}{variables used to create paths.}

\item{data_to}{folder where the new "data" folders will be created.}

\item{results_to}{folder where the new "results" folders will be created.}
}
\value{
new folders to save data and results
}
\description{
Create folders for data and results
}
\examples{
\dontrun{
}
}
