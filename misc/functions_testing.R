# Testing functions
# 2023-10-12


# Load packages -----------------------------------------------------------

library(tidyverse)


# Set paths ---------------------------------------------------------------

path_to_fam <- "../../../01-projects/projects/exige_test/raw/BWP3ext_420K_clean_mapped.fam"
# path_to_ped <- "../../../01-projects/projects/exige_test/raw/BWP3ext_420K_clean_mapped.ped"


# extract_fid -------------------------------------------------------------

extract_fid <- function(path, output = NULL) {

  fid_list <- vroom::vroom(path, col_names = FALSE, col_select = 1) |>
    dplyr::pull(X1)

  if (!is.null(output)) {
    vroom::vroom_write(fid_list, output)
  }

  return(fid_list)

}

# extract_iid -------------------------------------------------------------

extract_iid <- function(path, output = NULL) {

  iid_list <- vroom::vroom(path, col_names = FALSE, col_select = 2) |>
    dplyr::pull(X2)

  if (!is.null(output)) {
    vroom::vroom_write(iid_list, output)
  }

  return(iid_list)

}

iid_list <- extract_iid(path = path_to_fam)

# extract_ids -------------------------------------------------------------

path <- path_to_fam
cols <- c("IID", "FID_IID")
output <- NULL

extract_ids <- function(path, cols = c("FID", "IID", "FID_IID"), output = NULL) {

  fid_list <- vroom::vroom(path, col_names = FALSE, col_select = 1) |>
    dplyr::pull(X1)

  iid_list <- vroom::vroom(path, col_names = FALSE, col_select = 2) |>
    dplyr::pull(X2)

  fid_iid_list <- tibble(
    FID = fid_list,
    IID = iid_list,
    FID_IID = fid_iid_list
  )

  ids_list <- fid_iid_list |>
    dplyr::select(cols)

  return(ids_list)

}

# count_alleles -----------------------------------------------------------

count_alleles <- function(geno, output) {

  plink <- readr::read_csv("exec_paths") |>
    dplyr::filter(prog == "plink") |>
    dplyr::pull(path)

  system2(plink, c("--bfile", geno, "--freqx", "--out", paste0(output, "allele_count.txt")))

  }
