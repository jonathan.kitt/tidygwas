# TIDYGWAS : A toolkit of R functions for Genome-Wide Association Studies

*Last updated 2024-03-14*

## Table of contents

- [Setup](#setup)
  - [Install and load the package](#install-and-load-the-package)
  - [Download PLINK and FaST-LMM](#download-plink-and-fast-lmm)
  - [Set the paths to PLINK and FaST-LMM](#set-the-paths-to-plink-and-fast-lmm)
  - [Install the qvalue package](#install-the-qvalue-package)
  - [Folder structure](#folder-structure)
- [Files](#files)
  - [Genotyping files](#genotyping-files)

## Setup

### Install and load the package

First, install the `devtools` package from CRAN using `install.packages("devtools")`

Download the latest development version of the package using the following code :\
`devtools::install_git("https://forgemia.inra.fr/jonathan.kitt/tidygwas")`

Load the package using `library(tidygwas)`

### Download PLINK and FaST-LMM

Download the executable files for [PLINK](https://www.cog-genomics.org/plink/) and [FaST-LMM](https://www.microsoft.com/en-us/download/details.aspx?id=52614).

Unzip the downloaded files (e.g. in a dedicated folder on your computer)

### Set the paths to PLINK and FaST-LMM

In the console, type the following code to set the paths to PLINK and FaST-LMM :\
`tidygwas::set_paths(path_to_plink = "C:/Programs/gwas_software/PLINK.1.90/plink.exe", path_to_fastlmm = "C:/Programs/gwas_software/FaSTLMM.207c.Win/Cpp_MKL/")`.

This will create a file named `exec_paths.csv` in your working directory. To check if the file was correctly created, use `read.csv("exec_paths.csv")` :

| name    | path                                                |
| ------- | --------------------------------------------------- |
| plink   | C:/Programs/gwas_software/PLINK.1.90/plink.exe      |
| fastlmm | C:/Programs/gwas_software/FaSTLMM.207c.Win/Cpp_MKL/ |

### Install the qvalue package

The `tidygwas` toolkit includes using functionalities from the `qvalue` Bioconductor package.

To install the `qvalue` package, run the following code:
  
`if (!require("BiocManager", quietly = TRUE)) install.packages("BiocManager")`
  
`BiocManager::install("qvalue")`

### Folder structure

We recommend using the following folder structure :

├── 01-DATA-RAW  
├── 02-SCRIPTS  
├── 03-DATA-PROCESSED  
├── 04-RESULTS  
├── 05-FIGS  

## Files

To run the analysis, two types of files are needed :

### Genotyping files

Genotyping files can be in binary format (.bed, .bim, .fam) or in text format (.ped, .map).
Computing time can be reduced by using the binary format. 
To recode text format genotyping files into binary format genotyping files, use the following code :
  
`recode_files(path = "01-DATA-RAW/geno", from = "text", to = "binary")`

By default, recoded files are saved in the same folder. To save the recoded files to a specified folder, use the 
following code : 

`recode_files(path = "01-DATA-RAW/geno", from = "text", to = "binary", output = "02-DATA-PROCESSED/")`
