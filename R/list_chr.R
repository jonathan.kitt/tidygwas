#' List chromosomes
#'
#' @param map path to the .map file
#' @param output where the list of chromosomes will be exported.
#'
#' @import dplyr
#' @import readr
#' @import vroom
#'
#' @return list of chromosomes
#' @export
#'
#' @examples
#' \dontrun{
#" list_chr(map = "geno.map", output = "data/chr_list.csv")
#' }

list_chr <- function(map, output) {

  chr_list <- vroom::vroom(map, col_names = FALSE, col_select = X1) |>
    dplyr::rename(chr = X1) |>
    dplyr::distinct(chr) |>
    dplyr::arrange(chr) |>
    vroom::vroom_write(output, delim = ",")

  return(chr_list)

  }
