#' Split genotypes
#'
#' @param geno genotypes (.bed, .bim, .fam format).
#' @param iter table of iterations (created with the create_iter() function).
#' @param ... variables used to split the data.
#' @param path main directory where the tables will be exported.
#'
#' @import dplyr
#' @import tidyr
#' @import purrr
#'
#' @return tables of genotypes
#' @export
#'
#' @examples
#' \dontrun{
#" split_geno(geno, iter, ..., path = "data/")
#' }

split_geno <- function(geno, iter, ..., path) {

  plink <- readr::read_csv("exec_paths.csv") |>
    dplyr::filter(name == "plink") |>
    dplyr::pull(path)

  paths <- iter |>
    dplyr::distinct(...) |>
    dplyr::arrange(...) |>
    tidyr::unite(col = "dir_path", sep = "/") |>
    dplyr::mutate(path_smp = paste0(path, dir_path, "/samples.txt"),
                  path_out = paste0(path, dir_path, "/geno")) |>
    dplyr::select(-dir_path) |>
    dplyr::mutate(geno = geno, .before = path_smp)

  purrr::pmap(paths,
              function(geno, path_smp, path_out) {
                system2(plink, c("--bfile", geno, "--keep", path_smp,
                                 "--allow-extra-chr",
                                 "--make-bed", "--out", path_out))
              })

  file.remove(list.files(path = path,
                         pattern = ".nosex",
                         full.names = TRUE, recursive = TRUE))

  # plink <- readr::read_csv("exec_paths") |>
  #   dplyr::filter(prog == "plink") |>
  #   dplyr::pull(path)

  # d1 <- iter |>
  #   dplyr::distinct(...) |>
  #   dplyr::arrange(...) |>
  #   tidyr::unite(col = "path_tmp", sep = "/", remove = FALSE) |>
  #   dplyr::mutate(path_samples = paste0(path, path_tmp, "/", subdir, path_tmp,
  #                                       "_samples.txt"),
  #                 path_output = paste0(path, path_tmp, "/", subdir, path_tmp)) |>
  #   dplyr::select(path_samples, path_output)

  # for (i in 1:nrow(d1)) {
  #   system2(plink, c("--bfile", geno, "--keep", d1$path_samples[i],
  #                    "--make-bed", "--out", d1$path_output[i]))
  #   file.remove(paste0(d1$path_output[i], ".log"))
  #   file.remove(paste0(d1$path_output[i], ".nosex"))
  # }
}
