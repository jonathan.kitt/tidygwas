#' Create folders for data and results
#'
#' @param df table from which the paths will be created.
#' @param ... variables used to create paths.
#' @param data_to folder where the new "data" folders will be created.
#' @param results_to folder where the new "results" folders will be created.
#'
#' @import dplyr
#' @import tidyr
#' @import fs
#'
#' @return new folders to save data and results
#' @export
#'
#' @examples
#' \dontrun{
#" create_dir(df, trial, trait, add_chr = FALSE, output = NULL)
#' }

create_dir <- function(df, ..., data_to, results_to) {

  df |>
    dplyr::distinct(...) |>
    dplyr::arrange(...) |>
    tidyr::unite(col = "dir_path", sep = "/") |>
    dplyr::mutate(path_to_data = paste0(data_to, dir_path),
                  path_to_results = paste0(results_to, dir_path)) |>
    tidyr::pivot_longer(cols = -dir_path,
                        names_to = "path_type",
                        values_to = "path") |>
    dplyr::distinct(path) |>
    purrr::map(.f = fs::dir_create)

  # paths <- iter |>
  #   dplyr::distinct(...) |>
  #   dplyr::arrange(...) |>
  #   tidyr::unite(col = "dir_path", sep = "/") |>
  #   dplyr::transmute(full_path = paste0(path, dir_path))

  # if (subdir == "both") {
  #   paths <- paths |>
  #     dplyr::slice(rep(1:n(), each = 2)) |>
  #     dplyr::mutate(supp_dir = rep(c("data", "results"), times = (n() / 2))) |>
  #     tidyr::unite(col = "path", sep = "/") |>
  #     dplyr::pull(path)
  # }

  # if (subdir == "data" | subdir == "results") {
  #   paths <- paths |>
  #     dplyr::mutate(supp_dir = subdir) |>
  #     tidyr::unite(col = "path", sep = "/") |>
  #     dplyr::pull(path)
  # }

  # if (subdir == "none") {
  #   paths <- paths |>
  #     dplyr::pull(full_path)
  # }

  # sapply(X = paths, FUN = dir.create, recursive = TRUE)

}
