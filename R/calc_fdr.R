#' Calculate False Discovery Rate
#'
#' @param iter table of iterations (created with the create_iter() function).
#' @param ... variables used to split the data.
#' @param output where the results will be saved.
#'
#' @import dplyr
#' @import vroom
#' @import tidyr
#'
#' @return Table with FDR values
#' @export
#'
#' @examples
#' \dontrun{
#" calc_fdr(iter, ..., output)
#' }

calc_fdr <- function(iter, ..., path, output) {

  d1 <- iter |>
    dplyr::distinct(...) |>
    tidyr::unite(col = "dir_path", sep = "/", remove = FALSE) |>
    dplyr::mutate(path_input = paste0(path, dir_path, "_REML.txt"))

  fdr_list <- list()

  for (i in 1:nrow(d1)) {

    gwas <- vroom::vroom(d1$path_input[i]) |>
      dplyr::select(SNP, Chromosome, Pvalue, Qvalue_recalc)

    below_thr <- length(which(gwas$Qvalue_recalc < 0.05))
    above_thr <- length(which(gwas$Qvalue_recalc > 0.05))

    if (below_thr == 0 | above_thr == 0) {
      FDR <- NA
    }

    if (below_thr != 0 & above_thr != 0) {
      FDR <- gwas |>
        dplyr::filter(Qvalue_recalc == 0.05) |>
        dplyr::pull(Pvalue)
    }

    if (length(FDR) == 0) {
      gwas_sub <- gwas |>
        dplyr::filter(Qvalue_recalc >= 0.01, Qvalue_recalc <= 0.1)

      gwas_sub_lm <- lm(formula = Pvalue ~ Qvalue_recalc, data = gwas_sub)

      FDR <- unname((gwas_sub_lm$coefficients[2] * 0.05) + gwas_sub_lm$coefficients[1])

    }

    fdr_list[[i]] <- FDR

    }

  fdr_tbl <- d1 |>
    dplyr::select(...) |>
    dplyr::mutate(FDR = unlist(fdr_list))

  readr::write_csv(fdr_tbl, output)

  }
