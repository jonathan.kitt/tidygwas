#' Count alleles
#'
#' @param geno genotypes (.bed, .bim, .fam format)
#' @param output directory where the table will be exported.
#'
#' @import dplyr
#' @import vroom
#'
#' @return .bed, .bim, .fam files for each chromosome
#' @export
#'
#' @examples
#' \dontrun{
#" count_alleles(geno, output = "results/")
#' }

count_alleles <- function(geno, output) {

  plink <- readr::read_csv("exec_paths.csv") |>
    dplyr::filter(name == "plink") |>
    dplyr::pull(path)

  system2(plink, c("--bfile", geno, "--allow-extra-chr",
                   "--freqx", "--out", paste0(output, "allele_count")))

  file.remove(paste0(output, "allele_count.log"))
  file.remove(paste0(output, "allele_count.nosex"))

  d1 <- vroom::vroom(paste0(output, "allele_count.frqx"))

  names(d1) <- c("chr", "snp", "a1", "a2",
                 "hom_a1", "het", "hom_a2",
                 "hap_a1", "hap_a2", "missing")

  vroom::vroom_write(d1, paste0(output, "allele_count.txt"), delim = ",")

  file.remove(paste0(output, "allele_count.frqx"))

  }
