#' Statistics after splitting data
#'
#' @param iter table of iterations (created with the create_iter() function).
#' @param ... variables used to split the data.
#' @param path main directory where the log files are located.
#' @param output where to save the stats (if NULL, table is displayed in console)
#' @param remove remove original log files (TRUE dy default)
#'
#' @import dplyr
#' @import tidyr
#' @import purrr
#' @import readr
#' @import stringr
#'
#' @return table of counts
#' @export
#'
#' @examples
#' \dontrun{
#" stats_split(geno = "raw/geno", iter, ..., path = "data/", remove = TRUE)
#' }

stats_split <- function(iter, ..., path, output = NULL, remove = TRUE) {

  d1 <- iter |>
    dplyr::distinct(...) |>
    dplyr::arrange(...) |>
    tidyr::unite(col = "dir_path", sep = "/") |>
    dplyr::mutate(path_log = paste0(path, dir_path, "/geno.log")) |>
    dplyr::distinct(dir_path, path_log)

  # log_stats <- purrr::map_vec(.x = d1$path_log,
  #                             .f = ~readr::read_lines(file = .x, skip = 28, n_max = 1) |>
  #                               stringr::str_split(pattern = "and", n = 2, simplify = TRUE)
  # )

  log_stats <- purrr::map_vec(.x = d1$path_log,
                              .f = ~readr::read_lines(file = .x) |>
                                tibble::as_tibble() |>
                                dplyr::filter(stringr::str_detect(string = value,
                                                                  pattern = "pass filters and QC")) |>
                                dplyr::pull() |>
                                stringr::str_split(pattern = "and", n = 2, simplify = TRUE)
  )

  # log_stats <- purrr::map_vec(.x = d1$path_log,
  #                             .f = ~readr::read_lines(file = .x) |>
  #                               tibble::as_tibble() |>
  #                               dplyr::mutate(keep = stringr::str_detect(string = value,
  #                                                                        pattern = "[0-9]+ variants and [0-9]+ people pass filters and QC.")) |>
  #                               dplyr::filter(keep == TRUE) |>
  #                               dplyr::pull(value) |>
  #                               stringr::str_split(pattern = "and", n = 2, simplify = TRUE)
  #                             )

  d1 <- d1 |>
    dplyr::mutate(total_snps = readr::parse_number(log_stats[, 1]),
                  total_samples = readr::parse_number(log_stats[, 2]))

  if (remove == TRUE) {
    file.remove(d1$path_log)
  }

  d1 <- d1 |>
    dplyr::select(name = dir_path, total_snps, total_samples)

  if (!is.null(output)) {
    readr::write_csv(d1, output)
  }

  else {
    return(d1)
  }

}
