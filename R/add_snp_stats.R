#' Add SNP stats to REML results
#'
#' @param iter iteration table.
#' @param ... variables used to split data.
#' @param path_pheno path to raw phenotyping data (tidy format).
#' @param path_input path to main input directory.
#' @param path_output path to main output directory.
#'
#' @import readr
#' @import vroom
#' @import dplyr
#' @import tidyr
#' @import purrr
#' @import stringr
#' @import data.table
#'
#' @return GWAS results with SNP stats.
#' @export
#'
#' @examples
#' \dontrun{
#" add_snp_statds(iter, trial, trait, path_pheno = "raw/pheno.csv", path_intput = "data/", path_output = "results/)
#' }

add_snp_stats <- function(iter, ..., path_pheno, path_input, path_output) {

  plink <- readr::read_csv("exec_paths.csv") |>
    dplyr::filter(name == "plink") |>
    dplyr::pull(path)

  d1 <- iter |>
    dplyr::distinct(...) |>
    dplyr::mutate(path_geno = paste0(path_input, trial, "/geno_filtered"),
                  path_tmp = paste0(path_output, trial, "/", trait, "_tmp.txt"),
                  path_geno_filtered = paste0(path_output, trial, "/", trait, "_geno_filtered"),
                  path_stats = paste0(path_output, trial, "/", trait, "_stats"))

  pheno <- vroom::vroom(path_pheno)

  for (i in 1:nrow(d1)) {
    vroom::vroom(path_pheno) |>
      dplyr::filter(trial == d1$trial[i],
                    trait == d1$trait[i],
                    value != -9) |>
      tidyr::drop_na(value) |>
      dplyr::select(FID, IID) |>
      readr::write_tsv(d1$path_tmp[i])
  }

  purrr::pmap(dplyr::select(d1, path_geno, path_tmp, path_geno_filtered),
              function(path_geno, path_tmp, path_geno_filtered) {

                system2(plink, c("--bfile", path_geno, "--keep", path_tmp,
                                 "--out", path_geno_filtered, "--make-bed"))
              })

  file.remove(c(paste0(d1$path_geno_filtered, ".log"),
                paste0(d1$path_geno_filtered, ".nosex"),
                d1$path_tmp))

  purrr::pmap(dplyr::select(d1, path_geno_filtered, path_stats),
              function(path_geno_filtered, path_stats) {

                system2(plink, c("--bfile", path_geno_filtered,
                                 "--freq counts", "--out", path_stats))
              })

  file.remove(c(paste0(d1$path_stats, ".log"),
                paste0(d1$path_stats, ".nosex"),
                paste0(d1$path_geno_filtered, ".bed"),
                paste0(d1$path_geno_filtered, ".bim"),
                paste0(d1$path_geno_filtered, ".fam")))

  d1 <- d1 |>
    dplyr::mutate(path_gwas = stringr::str_replace_all(string = path_tmp,
                                                       pattern = "tmp",
                                                       replacement = "REML_R2LR"),
                  path_output = stringr::str_replace_all(string = path_tmp,
                                                         pattern = "tmp",
                                                         replacement = "REML"))

  for (i in 1:nrow(d1)) {
    data_gwas <- vroom::vroom(d1$path_gwas[i])
    data_stats <- data.table::fread(paste0(d1$path_stats[i], ".frq.counts"))

    data_merge <- data_gwas |>
      dplyr::left_join(data_stats) |>
      dplyr::rename(Count_A1 = C1,
                    Count_A2 = C2,
                    Count_NA = G0) |>
      dplyr::select(SNP:N, A1, A2, Count_A1:Count_NA, DOF:SNPCount) |>
      dplyr::mutate(Count_A1 = Count_A1 / 2,
                    Count_A2 = Count_A2 / 2) |>
      vroom::vroom_write(d1$path_output[i])

    file.remove(c(paste0(d1$path_stats[i], ".frq.counts"),
                  d1$path_gwas[i]))
  }
}
