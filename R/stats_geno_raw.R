#' Statistics on raw genotyping data
#'
#' @param path_to_geno path to genotyping files (without the extension).
#' @param path_to_output where to save the stats (if NULL, table is displayed in console)
#'
#' @import dplyr
#' @import readr
#'
#' @return table of counts for markers
#' @export
#'
#' @examples
#' \dontrun{
#" stats_geno_raw(path_to_geno = "raw/geno", path_to_output = "results/stats/")
#' }

stats_geno_raw <- function(path_to_geno, path_to_output) {

  plink <- readr::read_csv("exec_paths.csv") |>
    dplyr::filter(name == "plink") |>
    dplyr::pull(path)

  system2(plink, c("--bfile", path_to_geno, "--allow-extra-chr",
                   "--freqx", "--out",
                   paste0(path_to_output, "stats_geno_raw")))

  file.remove(paste0(path_to_output, "stats_geno_raw.nosex"))

  d1 <- vroom::vroom(paste0(path_to_output, "stats_geno_raw.frqx")) |>
    dplyr::rename(COUNT_HOM_A1 = 'C(HOM A1)',
                  COUNT_HET = 'C(HET)',
                  COUNT_HOM_A2 = 'C(HOM A2)',
                  COUNT_HAP_A1 = 'C(HAP A1)',
                  COUNT_HAP_A2 = 'C(HAP A2)',
                  COUNT_MISSING = 'C(MISSING)')

  if (is.null(path_to_output)) {
    return(d1)
    } else {
    readr::write_csv(d1, paste0(path_to_output, "stats_geno_raw.csv"))
      }

  file.remove(paste0(path_to_output, "stats_geno_raw.frqx"))
}
