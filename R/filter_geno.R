#' Filter data
#'
#' @param iter table of iterations (created with the create_iter() function).
#' @param ... variables used to split the data.
#' @param ext_input extension of input files.
#' @param thr_maf threshold
#' @param thr_geno threshold
#' @param thr_mind threshold
#' @param path main directory where the tables will be exported.
#' @param ext_output extension of output files.
#'
#' @import dplyr
#' @import stringr
#' @import tidyr
#' @import readr
#' @import purrr
#'
#' @return filtered .bed, .bim and .fam files
#' @export
#'
#' @examples
#' \dontrun{
#" filter_geno(iter, ..., ext_input = "", maf = 0.05, geno = 0.2, mind = 0.2,
#" path = "trials/", subdir = "data/", ext_output = "filtered")
#' }

filter_geno <- function(iter, ..., ext_input = "",
                        thr_maf, thr_geno, thr_mind,
                        path, ext_output = "filtered") {

  plink <- readr::read_csv("exec_paths.csv") |>
    dplyr::filter(name == "plink") |>
    dplyr::pull(path)

  d1 <- iter |>
    dplyr::distinct(...) |>
    dplyr::arrange(...) |>
    tidyr::unite(col = "dir_path", sep = "/") |>
    dplyr::mutate(path_geno = paste0(path, dir_path, "/geno", "_", ext_input),
                  path_output = paste0(path_geno, "_", ext_output)) |>
    dplyr::mutate(path_geno = stringr::str_remove_all(path_geno, "_$"),
                  path_output = stringr::str_replace_all(path_output, "_+", "_")) |>
    dplyr::distinct(dir_path, path_geno, path_output) |>
    dplyr::select(-dir_path) |>
    dplyr::mutate(thr_maf = thr_maf,
                  thr_geno = thr_geno,
                  thr_mind = thr_mind)

  purrr::pmap(d1,
              function(path_geno, thr_maf, thr_geno, thr_mind, path_output) {
                system2(plink, c("--bfile", path_geno, "--make-bed",
                                 "--maf", thr_maf, "--geno", thr_geno,
                                 "--mind", thr_mind,
                                 "--out", path_output))
              })

  file.remove(list.files(path = path,
                         pattern = ".nosex",
                         full.names = TRUE, recursive = TRUE))

  # plink <- readr::read_csv("exec_paths") |>
  #   dplyr::filter(prog == "plink") |>
  #   dplyr::pull(path)

  # d1 <- iter |>
  #   dplyr::distinct(...) |>
  #   dplyr::arrange(...) |>
  #   tidyr::unite(col = "path_tmp", sep = "/", remove = FALSE) |>
  #   dplyr::mutate(path_input = paste0(path, path_tmp, "/", subdir, path_tmp, "_", ext_input),
  #                 path_output = paste0(path_input, "_", ext_output)) |>
  #   dplyr::mutate(path_input = stringr::str_remove_all(path_input, "_$"),
  #                 path_output = stringr::str_replace_all(path_output, "_+", "_")) |>
  #   dplyr::select(-path_tmp)

  # for (i in 1:nrow(d1)) {
  #   system2(plink, c("--bfile", d1$path_input[i], "--make-bed", "--maf", maf,
  #                    "--geno", geno, "--mind", mind, "--out", d1$path_output[i]))
  #   file.remove(paste0(d1$path_output[i], ".log"))
  #   file.remove(paste0(d1$path_output[i], ".nosex"))
  #
  # }
  }
